package com.haemol.karma

object Cli {

  def main(args: Array[String]): Unit = {
    if (args.isEmpty) {
      println("Please provide rolls for which to calculate karma.")
      return
    }

    val rolls: Seq[Int] = args.map(_.toIntOption.getOrElse({
      println("Only integers representing rolls allowed as input.")
      return
    })).toSeq

    rolls.foreach(a => {
      if (3 > a || a > 18) {
        println(s"Found invalid attribute: $a. Only rolls between 3 and 18 are valid.")
        return
      }
    })

    val karmaCalculator = new KarmaCalculator
    println(s"Karma: ${karmaCalculator.calculate(rolls)}")
  }
}
