package com.haemol.karma

import scala.collection.MapView

object AttributeProbabilities {

  /**
   * Found on:
   * https://www.dandwiki.com/wiki/Character_Stat_Probabilities_(3.5e_Other)
   *
   * I manually confirmed for rolls of 18s and 17s, so I assume they are correct.
   */
  lazy val rollCombinations: Map[Int, Int] = Map(
    3 -> 1,
    4 -> 4,
    5 -> 10,
    6 -> 21,
    7 -> 38,
    8 -> 62,
    9 -> 91,
    10 -> 122,
    11 -> 148,
    12 -> 167,
    13 -> 172,
    14 -> 160,
    15 -> 131,
    16 -> 94,
    17 -> 54,
    18 -> 21
  )

  lazy val majorisationCombinations: Map[Int, Int] = {
    rollCombinations.keys.map(roll => {
      val totalCombinations = rollCombinations
        .filter(_._1 >= roll)
        .values
        .sum

      roll -> totalCombinations
    }).toMap
  }

  lazy val majorisationProbabilities: MapView[Int, Double] =
    majorisationCombinations.view.mapValues(majorisationProbability)

  private def majorisationProbability(combinations: Int): Double = combinations.toDouble / 1296.0
}
