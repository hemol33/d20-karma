package com.haemol.karma

import com.haemol.karma.AttributeProbabilities.majorisationProbabilities

class KarmaCalculator {

  def calculate(rolls: Seq[Int]): Double = {
    val probabilitiesCalculator = new UnorderedMajorisationCalculator

    val karmaMax = 1 - probabilitiesCalculator.calculate((1 to rolls.size).map(_ => majorisationProbabilities(18)))

    (1 - probabilitiesCalculator.calculate(rolls.map(majorisationProbabilities)))/karmaMax
  }

}
