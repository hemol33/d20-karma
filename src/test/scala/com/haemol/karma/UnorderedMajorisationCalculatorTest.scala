package com.haemol.karma

import org.scalatest.WordSpec
import org.scalatest._

class UnorderedMajorisationCalculatorTest extends WordSpec with Matchers {

  lazy val calculator = new UnorderedMajorisationCalculator

  "UnorderedMajorisationCalculator" should {
    "respect impossible events" in {
      calculator.calculate(Seq(0.0)) shouldEqual 0.0
      calculator.calculate(Seq(0.0, 0.0)) shouldEqual 0.0
      calculator.calculate(Seq(0.0, 0.0, 0.0, 0.0, 0.0)) shouldEqual 0.0
    }

    "respect necessary events" in {
      calculator.calculate(Seq(1.0)) shouldEqual 1.0
      calculator.calculate(Seq(1.0, 1.0)) shouldEqual 1.0
      calculator.calculate(Seq(1.0, 1.0, 1.0, 1.0, 1.0)) shouldEqual 1.0
    }

    "calculate some easy examples" in {
      calculator.calculate(Seq(0.5, 0.5)) shouldEqual 0.25
      calculator.calculate(Seq(0.5, 1)) shouldEqual 0.75
    }
  }

}
