# D20 Karma Calculator

This little program calculates "karma" of attribute rolls in 
d20 systems following dnd3.5 roll system - roll 4, drop lowest.

Karma is equivalent to negation of probability of rolls being
equal or higher. If only 5% of rolls would yield better outcome,
the karma will be 95%.
We do not compare sum of attributes, but rolls separately.
Karma value is scaled to cover full `[0, 1]` interval. 

This system promotes more natural rolls. 
Extreme values and absence of low rolls will yield high karma. 

## Usage

Use script with any number of rolls

```
./run x y z
```

should the script lack permissions use

```
chmod a+x run.sh 
```
